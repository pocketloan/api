const erroMessages = {
    GET_USER_BY_ID_ERROR: {
        message: 'error in fetching user by id',
        status: 404,
        title: 'user fetch by id error'
    },
    CREATE_USER_ERROR: {
        message: 'error occured when trying to create user',
        status: 400,
        title: 'create user error'
    },
    UNHEALTHY_APP: {
        message: 'app is unhealthy. it cannot connect to th database',
        status: 404,
        title: 'unhealthy app'
    },
    DELETE_USER_ERROR: {
        message: 'error occured when trying to delete user',
        staus: 400,
        title: 'error on user delete'
    },
    UPDATE_USER_ERROR: {
        message: 'error occured when trying to update user',
        staus: 404,
        title: 'error on user update'
    },
    USER_LOGIN_ERROR: {
        message: 'error occured during user authentication',
        staus: 404,
        title: 'invalid user'
    },
    USER_LOGIN_INVALID_PASSWORD: {
        status: 404,
        message: 'invalid password',
        success: false
    },
    UNAUTHORIZED_REQUEST: {
        status: 404,
        message: 'Unauthorized Request'
    },
    UPLOAD_ERROR: {
        status: 500,
        message: 'File Upload Error'
    },
    SERVER_ERROR: {
        status: 500,
        message: 'Internal Server Error'
    }
}
// Exports all error messages used in the application
module.exports = erroMessages
