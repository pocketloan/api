// Require dependencies
const Router = require('koa-router')
const { merge, isEmpty } = require('lodash')
const { version, hasAccess } = require('../../config')
const UserService = require('../services/user')
const log = require('custom-logger')
const {
    CREATE_USER_ERROR,
    DELETE_USER_ERROR,
    USER_LOGIN_ERROR,
    GET_USER_BY_ID_ERROR,
    UNAUTHORIZED_REQUEST,
    SERVER_ERROR
} = require('../errorMessages')

let UserDao = require('../dao/user')

/** @class {Object} UserRouter
 * @param {Object} options
 * performes user operation PUT, POST, GET, PATCH 
*/
class UserRouter extends Router {
    constructor(options = {}) {
        super()
        const { PermissionService, NotificationService, ActivityService } = options
        this.permissionService = new PermissionService({ UserDao })
        this.notificationService = new NotificationService()
        this.userService = new UserService()
        this.activityService = new ActivityService()
    }
    // initializes the router
    init(app) {
        this.prefix(`${version}`)
        this.get('/users', ctx => this.getUsers(ctx))
        this.get('/users/:id', ctx => this.getUserById(ctx))

        this.post('/users', ctx => this.createUser(ctx))
        this.post('/users/login', ctx => this.userLogin(ctx))
        this.post('/users/subscription', ctx => this.emailSubscriptions(ctx))
        this.post('/users/verify', ctx => this.verify(ctx))

        this.delete('/users/:id', ctx => this.deleteUser(ctx))
        this.patch('/users', ctx => this.updateUser(ctx))

        app.use(this.routes())
    }
    /**
     * @param {ctx} ctx route context object
     * get route to get all users
    */
    async getUsers(ctx) {
        try {
            const query = ctx.query
            const unFilteredUsers = await UserDao.getUsers()
            const allUsers = await this.userService.extractRequiredFields(unFilteredUsers, query)
            const users = await this.userService.querySearch(allUsers, query)
            ctx.body = {
                users,
                message: 'successfully fetched user(s)',
                status: 200
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in geting users`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * @param {ctx} ctx route context object
     * get route to get a specific user by id
   */
    async getUserById(ctx) {
        try {
            const { id } = ctx.params
            const unFilteredUser = await UserDao.getUserById(id)
            const user = await this.userService.extractRequiredFields(unFilteredUser, ctx.query)
            ctx.body = {
                message: 'successfully got user by id',
                status: 200,
                user
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in geting user by id`)
            ctx.status = 500
            ctx.body = GET_USER_BY_ID_ERROR
            throw err
        }
    }

    /**
     * @param {ctx} ctx route context object
     * delete route to delete specific users
    */
    async deleteUser(ctx) {
        try {
            const { id } = ctx.params
            await UserDao.deleteById(id)
            ctx.body = {
                message: 'user successfully deleted',
                status: 200
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in deleting user ${query} by ${field}`)
            ctx.status = 500
            ctx.body = DELETE_USER_ERROR
            throw err
        }
    }

    /**
     * @param {ctx} ctx route context object
     * patch route to update specific user
    */
    async updateUser(ctx) {
        try {
            const { field, query, id } = ctx.request.body
            const unFilteredUser = await UserDao.updateUser(field, query, id)
            const user = await this.userService.extractRequiredFields(unFilteredUser, ctx.query)
            await this.activityService.updateUserActivity({
                userId: id,
                fields: { title: 'Account Updated', content: `Updated${this._buildFields(field)} field(s) in account` }
            })
            ctx.body = {
                message: 'user successfully updated',
                status: 200,
                user
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in updating user ${query} by ${field}`)
            ctx.status = ctx.status = 500
            ctx.body = UPDATE_USER_ERROR
            throw err
        }
    }

    /**
     * @param {ctx} ctx route context object
     * post route to create a new users
    */
    async createUser(ctx) {
        try {
            const newUser = await this.userService.verifyAndCreate(ctx.request.body)
            const { status } = newUser.response
            if (status === 200 && !isEmpty(newUser.user)) {
                const content = `Account created on ${new Date().toISOString()}`
                await this.activityService.updateUserActivity({
                    userId: newUser.user._id,
                    fields: { title: 'User Accout Created', content }
                })
                await this.activityService.updateTransactionActivity({
                    transactionId: newUser.user.transactionId,
                    fields: { title: 'Transaction Accout Created', content }
                })
                ctx.body = await this._buildAccessToken(newUser)
            } else {
                ctx.body = newUser.response
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in creating a new user`)
            ctx.status = 500
            ctx.body = CREATE_USER_ERROR
            throw err
        }
    }

    /**
     * @param {Object} ctx route context object
     * post route to handle user login
    */
    async userLogin(ctx) {
        const { field, password } = ctx.request.body
        try {
            let loginResponse = await this.permissionService.ensure({ field, password })
            loginResponse = await this._rebuildResponse(loginResponse, ctx.query)
            ctx.body = loginResponse
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            log.debug(`err in verifying user credentials (user login)`)
            ctx.status = 500
            ctx.body = USER_LOGIN_ERROR
            throw err
        }
    }


    async emailSubscriptions(ctx) {
        try {
            const { email, firstName } = ctx.request.body
            const emailObj = await UserDao.emailSubscription(email)
            const template = 'tem_BR3M3YT8D64q9jY6twBqVWT3'
            const message = 'Thanks for subscribing for pocketloan we would definitely let you know when we offically launch'
            await this.notificationService.sendEmail(template, email, firstName, message)
            await this.notificationService.slackBot(`hurray! 🎊 ${email} just subscribed for pocketloan`)
            ctx.body = {
                status: 200,
                message: 'email successfully saved to subscription list',
                emailObj
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            ctx.status = 500
            ctx.body = UNAUTHORIZED_REQUEST
            throw err
        }
    }

    async verify(ctx) {
        try {
            let isValid = false, key = 0
            const { id } = ctx.request.body
            let users = await UserDao.getUsers()
            if (!isEmpty(id)) {
                users = await this.userService.extractRequiredFields(users, ctx.query)
                users.forEach(async (user, index) => {
                    if (id.toString() === user._id.toString()) {
                        isValid = true
                        await this.activityService.updateUserActivity({
                            userId: user._id.toString(),
                            fields: { title: 'User Accout Verified', content: 'Account was successfully verified' }
                        })
                        await UserDao.updateUser({ verified: true }, '', id)
                        key = index
                    }
                })
            }
            ctx.body = (isValid)
                ? { status: 200, message: `your account is now verified`, user: users[key] }
                : { status: 404, message: `userId is invalid `, user: null }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-api err: ${err.stack.toString()}`)
            ctx.status = UNAUTHORIZED_REQUEST.status
            ctx.body = UNAUTHORIZED_REQUEST
            throw err
        }
    }

    /**
     * @param {Object} loginResponse user login response object
     * @param {String} ctx.query koa context query object
     * @returns {Object} rebuilt loginResponse with tokenized user
    */
    async _rebuildResponse(loginResponse, query) {
        if (loginResponse.user) {
            let { user } = loginResponse
            user = await this.userService.extractRequiredFields(user, query)
            await this.activityService.updateUserActivity({
                userId: user._id,
                fields: { title: 'Last Logged Session', content: `Last logged in on ${new Date().toISOString()}` }
            })
            user = await this.permissionService.signJwt(user)
            loginResponse.user = user
        }
        return loginResponse
    }

    /**
     * @param {Object} creatUserResponse create user response object
     * @param {Object} ctx.query koa context query object
     * @returns {Object} rebuilt creatUserResponse with accessToken for immediate login
     */
    async _buildAccessToken({ response, user }) {
        const { _id } = user
        const accessToken = await this.permissionService.signJwt(merge({ userId: _id }, hasAccess[3]))
        await this._sendWelcomeEmail(user)
        return merge({ accessToken }, response)
    }
    /**
     * @param {Object} user created user object
     * Sends a welcome email to the user
    */
    async _sendWelcomeEmail(user) {
        const { email, firstName } = user
        const templateId = 'tem_VbmXdTTHPSFXjVvchFCSvQSY'
        const welcomeMessage = 'welcome to pocketloan, verify your account and start borrowing and loaning with ease'
        const emailUser = await this.notificationService.sendEmail(templateId, email, firstName, welcomeMessage, user._id)
        await this.notificationService.slackBot(`hurray! 🎊 a new user ${firstName} just signed up for pocketloan`)
        log.info(emailUser.data.message)
    }

    _buildFields(fields) {
        let result = ''
        for (let field in fields) {
            result += ` ${field},`
        }
        result = result.slice(0, -1)
        return result
    }
}
// Exports the UserRouter class
module.exports = UserRouter
