const UserRouter = require('./user')
const HealthRouter = require('./health')
const HomeRouter = require('./home')
const OpenApiRouter = require('./openapi')
const Middleware = require('../services/middleware')
const PermissionService = require('../services/permission')
const NotificationService = require('../services/notification')
const ActivityService = require('../services/activity')

// Exports and Initializes the application routes
module.exports = (app) => {
  const handlers = [
    new HealthRouter(),
    new OpenApiRouter(),
    new HomeRouter({ Middleware }),
    new UserRouter({
      PermissionService,
      NotificationService,
      ActivityService
    })
  ]

  handlers.forEach(handler => {
    handler.init(app)
  })
}
