// Require dependencies
const mongoose = require('mongoose')

// creates and intitializes the subscription schema
const SubscriptionSchema = new mongoose.Schema({
    dateCreated: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        required: true
    }
})

// Exports the subscription model schema
module.exports = mongoose.model('Subscription', SubscriptionSchema)
