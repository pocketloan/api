// Require dependencies
const mongoose = require('mongoose')

// creates and intitializes the user schema
const UserSchema = new mongoose.Schema({
  accountType: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: true,
    index: {
      unique: true
    }
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  sex: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  socialInsuranceNumber: {
    type: String
  },
  postalCode: {
    type: String,
    required: true
  },
  age: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
    required: true
  },
  dateCreated: {
    type: Date,
    default: Date.now
  },
  dateUpdated: {
    type: Date,
    default: Date.now
  },
  rating: {
    type: String,
    required: true
  },
  transactionId: {
    type: String,
    required: true
  },
  token: {
    type: String
  },
  verified: {
    type: Boolean,
    default: false
  },
  other: [
    {
      type: String,
      name: String
    }
  ]
})

// Exports the user model schema
module.exports = mongoose.model('User', UserSchema)
