const _ = require('lodash')
const log = require('custom-logger')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { jwtSecret, hasAccess } = require('../../config')
const { USER_LOGIN_INVALID_PASSWORD } = require('../errorMessages')

/**
 * @class {Object} PermissionService
 * @param {Object} options
 * ensures user has access to database recources authorized for
*/
class PermissionService {
    constructor(options = {}) {
        this.options = options
        const { UserDao } = options
        this.userDao = UserDao
    }

    /**
     * @param {}
     * @returns {Object} ensures user exists
     * verifies if a user exists by checking username and email
    */
    async ensure(config) {
        const { field, password } = config
        const checkUserName = await this.userDao.getUser('userName', field)
        const checkEmail = await this.userDao.getUser('email', field)
        if (_.isEmpty(checkUserName) && _.isEmpty(checkEmail)) {
            return {
                message: 'invalid username or email',
                sucess: false,
                status: 404
            }
        }
        if (!_.isEmpty(checkUserName)) {
            return await this.verifyPassword(password, checkUserName[0].password, checkUserName[0])
        }
        if (!_.isEmpty(checkEmail)) {
            return await this.verifyPassword(password, checkEmail[0].password, checkEmail[0])
        }
    }

    /**
     * @param {Object} users all users of pocketloan
     * @returns {[Sting]} an array of jwt token
     * converts an array of user objects to string of jwt token for security
    */
    async encryptUsers(users) {
        const result = []
        users.forEach(async (user) => {
            const token = await this.generateJwtToken(user, jwtSecret, 3600)
            result.push(token)
        })
        return result
    }

    /**
     * @param {String} password
     * @param {String} encryptedPassword
     * @returns {Object} ensures user exists
     * compares two passwords and verifies authenticity of the user
    */
    async verifyPassword(password, encryptedPassword, user) {
        return bcrypt.compare(password, encryptedPassword).then(async (isMatch) => {
            if (isMatch) {
                log.info('user successfully logged in')
                const { _id } = user
                const payload = _.merge({ userId: _id }, hasAccess[3])
                const accessToken = await this.signJwt(payload)
                return {
                    status: 200,
                    message: 'valid user credentials',
                    user,
                    accessToken
                }
            } else {
                return USER_LOGIN_INVALID_PASSWORD
            }
        }).catch(err => { throw err })
    }

    /**
     * @param {Object} payload
     * @param {String} jwtSecret
     * @param {Int} expiresIn
     * @returns {String} a user jwt token
     * signs and creates a jwt from payload
    */
    async signJwt(payload) {
        let token
        try {
            token = jwt.sign(payload, jwtSecret, { expiresIn: 3600 })
        } catch (err) {
            if (err) {
                token = jwt.sign(payload.toJSON(), jwtSecret, { expiresIn: 3600 })
                log.error(`${err.message}`)
            }
        }
        return token
    }

    /**
     * @param {}
     * @return {Object} options
     * method to return permission options
    */
    async getOptions() {
        return this.options
    }
}

module.exports = PermissionService