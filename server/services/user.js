const _ = require('lodash')
const UserSchema = require('../models/user')
const mongoose = require('mongoose')
const UserDao = require('../dao/user')

/**
 * @class {Object} Users
 * @param {Object} UserObject
 * this constructor handles the CRUD on userObjects
*/
class UserService {
  constructor(options = {}) {
    this.options = options
  }
  /**
   * @param {Object} newUser
   * @return {Object} users and response field
   * verifies and create user authencity
  */
  async verifyAndCreate(newUser) {
    const response = {}
    const requiredFields = [
      'firstName',
      'lastName',
      'accountType',
      'userName',
      'email',
      'password',
      'address',
      'rating',
      'age',
      'phoneNumber',
      'sex',
      'postalCode'
    ]
    requiredFields.forEach(field => {
      if (_.isNil(newUser[field])) {
        newUser[field] = 'empty'
      }
    })
    const allUsers = await UserDao.getUsers()
    const isUnique = await this.isUserUnique(newUser, allUsers)
    let createdUser
    if (isUnique.result) {
      createdUser = await UserDao.createUser(newUser)
      response.message = 'new user successfully created'
      response.status = 200
    } else {
      response.message = isUnique.message
      response.status = 409
    }
    return {
      response,
      user: (isUnique.result && !_.isEmpty(createdUser)) ? createdUser : {}
    }
  }
  /**
   * @param {Object} newUser
   * @param {[Object]} allUsers
   * @return {Boolean} result
   * checks if th user is unique
  */
  async isUserUnique(newUser, allUsers) {
    let response = {}
    response.result = true
    allUsers.forEach(user => {
      if ((user.userName === newUser.userName) || (newUser.email === user.email)) {
        response.result = false
        response.message = (user.userName === newUser.userName)
          ? `username ${newUser.userName} is not unique`
          : `email ${newUser.email} already exists`
      }
    })
    return response
  }


  /**
   * @param {Object} userObject 
   * @returns {Object}
   * extracts required filed from the userObject
  */
  async extractRequiredFields(users, claims = {}) {
    if (this._isPocketloanService(claims)) {
      return users
    }
    const required = ['_id', 'firstName', 'lastName', 'avatar', 'userName', 'email', 'sex', 'accountType', 'age']
    const fields = required.concat(['rating', 'verified', 'phoneNumber', 'address', 'dateCreated', 'dateUpdated'])
    if (_.isArray(users)) {
      let result = []
      users.forEach(user => {
        result.push(_.pick(user, fields))
      })
      return result
    }
    return _.pick(users, fields)
  }

  /**
   * @param {Object} users - list of all users
   * @param {Object} params - query params for elastic search
   * @returns {Object}
   * extracts required filed from the userObject
  */
  async querySearch(users, params) {
    const result = []
    const allowedFields = ['firstName', 'lastName', 'userName', 'email', 'sex', 'age', 'rating', 'verified', 'accountType']
    if (_.isEmpty(_.pick(params, allowedFields))) {
      return users
    }
    allowedFields.forEach(field => {
      users.forEach(user => {
        if(!_.isEmpty(params[field]) && params[field].toString().toLowerCase() === user[field].toString().toLowerCase()) {
          result.push(user)
        }
      })
    })
    return result
  }

  _isPocketloanService(claims = {}) {
    if (!_.isEmpty(claims)) {
      const { subType, sub, iss } = claims
      return subType === 'service' && sub.includes('pocketloan') && iss.includes('pocketloan')
    }
  }
}

// Exports the UserController
module.exports = UserService
