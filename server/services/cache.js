const NodeCache = require('node-cache')
const { version, pocketloanAuthService } = require('../../config')
const log = require('custom-logger')

/**
 * @class {Object} CacheService
 * @param {Object} options
 * caches important information in application memory to improve speed
 * and minimize database requests
*/
class CacheService {
    constructor(options = {}) {
        const { ttlSeconds } = options
        this.cache = new NodeCache({
            stdTTL: ttlSeconds,
            checkperiod: ttlSeconds * 0.2,
            useClones: false
        })
    }

    /**
     * @param {String} key
     * @param {Function} storeFunction
     * @returns {Object} value
     * returns the object that was cached using the key as identifier
    */
    get(key) {
        key = String(key)
        const value = this.cache.get(key)
        return (value) ? value : ''
    }

    /**
     * @param {String} key
     * @param {Object} result
     * set cahes caches an object taking a key as identifier
    */
    set(key, result) {
        key = String(key)
        this.cache.del(key)
        this.cache.set(key, result)
    }

    /**
     * @param {String} key
     * @param {Object} result
     * set update cached object 
    */
    update(key, result) {
        key = String(key)
        const user = this.get(key)
        if (user) {
            this.cache.del(key)
            this.set(key, result)
        }
    }

    /**
     * @param {String} startStr
     * @param {Object} result
     * set cahes caches an object taking a key as identifier
    */
    delStartWith(startStr = '') {
        if (!startStr) {
            return;
        }

        const keys = this.cache.keys();
        for (const key of keys) {
            if (key.indexOf(startStr) === 0) {
                this.cache.del(key)
            }
        }
    }

    /**
     * @param {}
     * flush empties cache
    */
    flush() {
        this.cache.flushAll()
    }

    /**
     * @param {}
     * @returns {Object} all cached items in memory
     * flush empties cache
    */
    getAll() {
        return this.cache.keys()
    }
    /**
     * @param {key} key
     * delete a cached object by key
    */
    delete(key) {
        key = String(key)
        this.cache.del(key)
    }
}

module.exports = CacheService

