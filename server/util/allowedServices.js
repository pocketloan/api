module.exports = [
    {
        sub: 'pocketloan-auth',
        subType: 'service',
        aud: 'pocketloan.api.internal',
        iss: 'pocketloan.auth.internal'
    },
    {
        sub: 'pocketloan-payments',
        subType: 'service',
        aud: 'pocketloan.api.internal',
        iss: 'pocketloan.payments.internal'
    },
    {
        sub: 'pocketloan-admin',
        subType: 'service',
        aud: 'pocketloan.api.internal',
        iss: 'pocketloan.admin.internal'
    },
    {
        sub: 'pocketloan-client',
        subType: 'service',
        aud: 'pocketloan.client.internal',
        iss: 'pocketloan.api.internal'
    },
    {
        sub: 'pocketloan-monitoring',
        subType: 'service',
        aud: 'pocketloan.api.internal',
        iss: 'pocketloan.monitoring.internal'
    }
]