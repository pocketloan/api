const axios = require('axios')
const jwt = require('jsonwebtoken')
const { hasAccess, jwtSecret } = require('../../config')

const setAuthHeaders = () => {
    const signedJwt = jwt.sign(hasAccess[0], jwtSecret, { expiresIn: 3600 })
    axios.defaults.headers.common['Authorization'] = signedJwt
    return axios
}

module.exports = setAuthHeaders()