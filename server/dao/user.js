// Require dependencies
const axios = require('../util/axios')
const bcrypt = require('bcryptjs')
const Mongoose = require('mongoose')
const gravatar = require('gravatar')
const User = require('../models/user')
const Subscription = require('../models/email')
const Mongo = require('./mongo')
const log = require('custom-logger')
const CacheService = require('../services/cache')
const ActivityService = require('../services/activity')
const _ = require('lodash')
const { pocketloanTransactions } = require('../../config')

/** @class {Object} Users Data Access Object
 *  @param {Object} options
 * Connects and fetches data from the database
 * All database requests should be gotten with the UserDoa 
 */
class UserDoa {
    constructor(options = {}) {
        const { CacheService } = options
        this.Mongo = new Mongo({ databaseName: 'pocketloan', keepAlive: true })
        this.mongoose = Mongoose
        this.connectToDatabase()
        const ttlSeconds = 60 * 60 * 1
        this.cache = new CacheService({ ttlSeconds })
        this.activityService = new ActivityService()
    }
    /**
     * @param {}
     * @return {Object} users
     * method to gets all users from the database
     */
    async getUsers() {
        const users = this.cache.get('users')
        if (!_.isEmpty(users) && !_.isNil(users)) {
            return users
        } else {
            const user = this.mongoose.model('User')
            return user.find({}, async (err, users) => {
                if (err) {
                    throw err
                }
                log.info('all users sucessfully cached')
                await this.cache.set('users', users)
            })
        }
    }
    /**
     * @param {Object} newUser
     * @return {Object} status
     * method to create a new user in the database
     */
    async createUser(newUser) {
        const user = this.mongoose.model('User')
        const create = new user(newUser)
        let transaction = { _id: '0123456789' }
        if (!process.env.TESTMODE) {
            let response = await this.createTransaction({ email: create.email, userId: create._id, country: create.country })
            transaction = response.transaction
            await this.activityService.createActivity({ transactionId: transaction._id, userId: create._id })
        }
        if (!_.isNil(newUser.password)) {
            create.transactionId = transaction._id
            create.password = await this._hashPassword(create)
            create.accountType = 'borrower'
            create.avatar = gravatar.url(newUser.email, {
                s: '250', // size
                r: 'pg', // rating
                d: 'identicon' // default
            })
            return create.save().then(user => {
                log.info('user sucessfully created')
                this.cache.delete('users')
                this.cache.set(create._id, create)
                return user
            }).catch(err => { throw err })
        }
    }
    /**
     * @param {String} field
     * @param {String} query
     * @return {Object} result
     * method to gets specific user by any feild from the database 
    */
    async getUser(field, query) {
        const result = []
        const allUsers = await this.getUsers()
        allUsers.forEach(user => {
            if (user[field] === query) {
                result.push(user)
            }
        })
        return result
    }
    /**
     * @param {String} _id
     * @return {Object} users
     * method to gets specific user by there ID from the database
     */
    async getUserById(id) {
        const users = this.mongoose.model('User')
        if (!_.isNil(id)) {
            return users.findById(id, (err, user) => {
                if (err) {
                    throw err
                }
                this.cache.set(user._id, user)
                log.info('user sucessfully cached by id')
                return user
            })
        }
    }
    /**
     * @param {String} _id
     * @return {Object} users
     * method to delete specific user by there ID  from the database
     */
    async deleteById(_id) {
        if (!_.isEmpty(_id)) {
            const users = this.mongoose.model('User')
            await this.deleteTransaction(_id)
            return users.deleteOne({
                _id
            }, (err) => {
                if (err) {
                    throw err
                }
                log.info(`user with id ${_id} was sucessfully deleted`)
                this.cache.delete(_id)
                this.cache.delete('users')
            })
        }
    }
    /**
     * @param {String} field
     * @param {String} query
     * method to delete specific users from the database
     */
    async deleteUser(field, query) {
        const users = await this.getUser(field, query)
        users.forEach(async (user) => {
            const { _id } = user
            await this.deleteById(_id)
            log.info('user sucessfully deleted')
        })
    }
    /**
     * @param {String} email
     * @return {String}
     * method to create / add a neww email to the email subscription list
     */
    async emailSubscription(email) {
        const subscription = this.mongoose.model('Subscription')
        const newEmail = new subscription({ email, dateCreated: new Date().toISOString() })
        return newEmail.save().then((email) => {
            return email
        })
    }
    /**
     * @param {String} field
     * @param {String} query
     * @param {String} _id
     * @return {Object} users
     * method to update specific field of the user from the database
     */
    async updateUser(field, query, _id) {
        if (!_.isNil(_id)) {
            const users = this.mongoose.model('User')
            if (_.isString(field)) {
                return users.findById(_id, (err, user) => {
                    if (err) {
                        throw err
                    }
                    user[field] = query
                    user['dateUpdated'] = new Date().toISOString()
                    user.save((err, updated) => {
                        if (err) {
                            throw err
                        }
                        this.cache.delete('users')
                        this.cache.update(_id, updated)
                        log.info('user sucessfully updated')
                        return updated
                    })
                })
            } else {
                return users.findById(_id, (err, user) => {
                    if (err) {
                        throw err
                    }
                    for (let prop in field) {
                        if (!_.isEmpty(field[prop])) {
                            user[prop] = field[prop]
                        }
                        if (field['verified'] === false || field['verified'] === true) {
                            user[prop] = field[prop]
                        }
                    }
                    user['dateUpdated'] = new Date().toISOString()
                    user.save((err, updated) => {
                        if (err) {
                            throw err
                        }
                        this.cache.delete('users')
                        this.cache.update(_id, updated)
                        log.info('user sucessfully updated')
                        return updated
                    })
                })
            }
        }
    }
    /**
     * @param {Object[String]} email
     * @param {Object[String]} userId
     * @param {Object[String]} country
     * creates a new transaction and stripe account for a user
     */
    async createTransaction({ userId, email, country = 'CA' }) {
        return axios.post(`${pocketloanTransactions}/v1/transactions/`, { userId, email, country })
            .then(response => response.data)
            .catch(err => { throw err })
    }
    /**
     * @param {String} id
     * deletes a transaction by id
     */
    async deleteTransaction(id) {
        if (!_.isEmpty(id)) {
            const user = await this.getUserById(id)
            if (!_.isEmpty(user.transactionId)) {
                return axios.delete(`${pocketloanTransactions}/v1/transactions/${user.transactionId}`)
                    .then(response => response.data)
                    .catch(err => { throw err })
            }
        }
    }
    /**
     * @param {Object} user
     * Hashes / encrypts a user's password 
     */
    async _hashPassword(user) {
        const password = user.password
        const saltRounds = 10
        const hashedPassword = await new Promise((resolve, reject) => {
            bcrypt.hash(password, saltRounds, (err, hash) => {
                if (err) reject(err)
                resolve(hash)
            });
        })
        return hashedPassword
    }

    /**
     * @param {}
     * @return {Object} MongooseConnection
     * method to connect users to the database
     */
    async connectToDatabase() {
        return await this.Mongo.connect()
    }
}

// exports the userDao
module.exports = new UserDoa({ CacheService })
