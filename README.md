# Pocketloan-Api
PocketLoan back-end system / logic for authentication and database connection

## Locally
### Install app depencies
``` 
npm install 
```
### Test Application
```
npm start && npm run test
```
### Start Application
```
npm start 
```
## Testing Locally In Docker Image
### Build Docker Image
```
docker build -t pocketloan-test -f Dockerfile-test .
```
### Run Tests In Docker Image
```
docker run --rm pocketloan-test
```

## Production
### Build Docker Image
```
docker build -t pocketloan . 
```

### Compose Docker Image
```
docker-compose up 
```

### Pocketloan-api Overview
![Scheme](images/pocketloanOverview.png)
