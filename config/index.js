const _ = require('lodash')
const allowedServices = require('../server/util/allowedServices')
const PORT = process.env.PORT || 3000

/**
 * {Object} exports app enviroment variables and secrets
 * eventuall this should all be in the docker container environment
*/
module.exports = {
    uri: process.env.MONGO_URI,
    version: process.env.VERSION,
    PORT,
    url: (!_.isNil(PORT)) ? `http://localhost:${PORT}` : 'http://localhost:3001',
    jwtSecret: process.env.JWTSECRET,
    pocketloanNotificationService: process.env.POCKETLOAN_NOTIFY,
    pocketloanAuthService: process.env.POCKETLOAN_AUTH,
    pocketloanTransactions: process.env.POCKETLOAN_TRANSACTIONS,
    pocketloanActivity: process.env.POCKETLOAN_ACTIVITY,
    env: (req) => (req.header.host.includes(`localhost:${PORT || 3000}`)) ? 'development' : 'production',
    hasAccess: allowedServices
}
