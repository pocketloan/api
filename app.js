// Require dependencies
const logger = require('koa-logger')
const json = require('koa-json')
const cors = require('koa2-cors')
const bodyParser = require('koa-bodyparser')
const koaSwagger = require('koa2-swagger-ui')
const { PORT, version } = require('./config')
const router = require('./server/routes')

/**
 * @class {Object} Index
 * @constructor {Object} koa router
 * @desc  Initializes the koa.js application, and all requirements needed
*/
class App {
    constructor(koa) {
        this.init(new koa())
    }

    // Injects dependencies into application
    init(app) {
        app.use(logger())
        app.use(cors())
        app.use(json())
        app.use(bodyParser())
        router(app)
        app.use(
            koaSwagger({
                routePrefix: `/swagger`, // host at /swagger instead of default /docs
                swaggerOptions: {
                    url: `/v1/openapi`, // example path to json
                },
            }),
        )
        this.home(app)
        this.listen(app)
    }

    /**
     * @param {Object} koa application
     * @desc  set default route for application
    */
    home(app) {
        app.use(ctx => {
            ctx.body = { message: `you shouldn't be here 🙃`, status: 404 }
        })
    }

    /**
     * @param {Object} koa application
     * @desc  creates port for the application to listen in on
    */
    listen(app) {
        app.listen(PORT, () => console.log(`pocketloan-api is running on port *${PORT}`))
    }
}

// Exports Index or application entry point
module.exports = App
