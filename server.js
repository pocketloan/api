/**
 @desc Require app dependencies
 @var {Instance} the koa Instance
 @class {Object} the app Index Obeject
*/
require('dotenv').config()
const koa = require('koa')
const app = require('./app')

/**
 @desc  Instantiate the koa app
 @class {Object} initializes our application
*/
new app(koa)
